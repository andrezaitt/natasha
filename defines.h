#ifndef DEFINES_H
#define DEFINES_H

#define PERIODIC_PATH "/home/nucleus/periodic"
#define SPORADIC_PATH "/home/nucleus/sporadic"

#define LOG_FILE PERIODIC_PATH"/restclient.log"

#endif
