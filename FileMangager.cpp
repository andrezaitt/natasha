#include "FileManager.h"

#include <QString>
#include <QJsonObject>

#include "file_system/JSonFile.h"

#define REST_CONFIG_FILE "/home/nucleus/sporadic/rest_config"

FileManager::FileManager()
        : json_file_(REST_CONFIG_FILE),
          rest_config_{"","","application/json"} {
    ParseJson();
}

FileManager::~FileManager() { }

void FileManager::ParseJson() {
    QJsonObject object = json_file_.GetObject();
    if (object.empty()) {
        return;
    }
    if (object.contains("rest_config")) {
        QJsonObject rest_object = object.take("rest_config").toObject();
        if (rest_object.contains("base_url")) {
            rest_config_.base_url = rest_object.take("base_url").toString()
                    .toStdString();
        }
        if (rest_object.contains("authorization")) {
            std::string authorization = rest_object.take("authorization")
                    .toString().toStdString();
            if (!authorization.empty())
                rest_config_.authorization = authorization;
        }
        if (rest_object.contains("content_type")) {
            std::string content = rest_object.take("content_type")
                    .toString().toStdString();
            if (!content.empty())
                rest_config_.content_type = content;
        }
    }
}

RestConfig FileManager::GetRestConfiguration() {
    return rest_config_;
}
