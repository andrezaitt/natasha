#include "RestClient.h"

#include <QByteArray>
#include <QJsonObject>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QPair>
#include <QObject>
#include <QUrl>

#include "rest_api/RestClientAdaptor.h"

namespace natasha {
namespace rest_api {

RestClient::RestClient(std::shared_ptr<spdlog::logger> logger,
                       const RestConfig rest_config)
        : url_base_(rest_config.base_url.c_str()),
          content_type_(rest_config.content_type),
          authorization_(rest_config.authorization),
          logger_(logger) {
    logger_->info("RestClient: Starting rest client.");
    new RestClientAdaptor(this);
    network_manager_ = new QNetworkAccessManager();
    connect(network_manager_, &QNetworkAccessManager::finished, this,
            &RestClient::ReplyFinished);
    QDBusConnection dbus = QDBusConnection::systemBus();
    bool conn_status = dbus.isConnected();
    logger_->debug("RestClient: Trying to connect to dbus session, status {}",
                   conn_status);

    bool reg_serv_status = dbus.registerService("org.natasha.restapi");
    bool reg_obj_status = dbus.registerObject("/RestClient", this);
    QDBusError error = dbus.lastError();
    if (!reg_serv_status || !reg_obj_status || !conn_status) {
        logger_->error("RestClient: Error in dbus session.");
        if (error.isValid())
            logger_->error("RestClient: Error registering dbus service: {}",
                           error.message().toStdString());
    } else {
        logger_->info("RestClient: Dbus session created with service name: {} "
                      "and object path: {}", "org.natasha.restapi",
                      "/RestClient");
    }
}

RestClient::~RestClient() {
    network_manager_->deleteLater();
}

void RestClient::Post(const QString &data, const QString &endpoint) {
    // send post request.
    logger_->info("RestClient: Creating post request to endpoint {}.",
                  url_base_.toStdString() + endpoint.toStdString());
    QString url_str(url_base_ + endpoint);
    QUrl url(url_str);
    QNetworkRequest request_url(url);
    request_url.setHeader(QNetworkRequest::ContentTypeHeader,
                          "application/json");
    if (!authorization_.empty())
        request_url.setRawHeader("authorization", authorization_.c_str());
    std::string payload_data = data.toStdString();
    logger_->info("RestClient: data to post -> {}", payload_data);
    QByteArray payload(payload_data.c_str());
    network_manager_->post(request_url, payload);
}

void RestClient::ReplyFinished(QNetworkReply *reply) {
    logger_->info("RestClient: Post request finished with code {0:d}.",
                  static_cast<int>(reply->error()));
    QList<QNetworkReply::RawHeaderPair> response = reply->rawHeaderPairs();
    int server_code = static_cast<int>(
            reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
    logger_->info("RestClient: Post response from server -> {0:d}.",
                  server_code);
    QByteArray response_data = reply->readAll();
    logger_->info("RestClient: Response data from server: {}",
                  response_data.toStdString());
}

}
}
