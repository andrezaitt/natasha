#ifndef RESTCLIENT_H
#define RESTCLIENT_H

#include <QByteArray>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>

#include <string>

#include "FileManager.h"
#include "spdlog/spdlog.h"

namespace natasha {
namespace rest_api {

class RestClient : public QObject {
     Q_OBJECT
     Q_CLASSINFO("D-Bus interface", "org.natasha.restapi")
 public:
     RestClient(std::shared_ptr<spdlog::logger> logger,
                const RestConfig rest_config);
     virtual ~RestClient();
     Q_SCRIPTABLE void Post(const QString &data, const QString &endpoint);

 private:
     void ReplyFinished(QNetworkReply *reply);
     QNetworkAccessManager *network_manager_;
     const QString url_base_;
     const std::string content_type_;
     const std::string authorization_;
     std::shared_ptr<spdlog::logger> logger_;
};

}
}

#endif
