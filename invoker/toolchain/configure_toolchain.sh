#!/bin/bash
RPI_ZIP_FILE=2018-04-18-raspbian-stretch.zip
RPI_IMG_FILE=2018-04-18-raspbian-stretch.img
TOOLCHAIN=tools
DARIUS_TOOLCHAIN=cross-compile-tools
QT=qt5
OFFSET=0
BLOCK_SIZE=512
function get_toolchain_tools() {
    if [[ -d $TOOLCHAIN ]]; then
        echo "** Found toolchain tools."
        return
    fi
    git clone https://github.com/raspberrypi/tools.git
}

function get_darius_toolchain() {
    if [[ -d $DARIUS_TOOLCHAIN ]]; then
        echo "** Found darius toolchain tools."
        return
    fi
    git clone https://github.com/darius-kim/cross-compile-tools.git
}
function get_qt5_module() {
    if [[ -d $QT ]]; then
        echo "** Found qt5 modules."
        return
    fi
    git clone git://code.qt.io/qt/qt5.git
    cd $QT
    ./init-repository
}

function get_rpi_img() {
    if [[ -e $RPI_IMG_FILE ]]; then
        echo "** Found good version wheezy image, skipping process."
        return
    fi
    if [[ -e $RPI_IMG_FILE ]]; then
        echo "** Found a zip file of RPI img, unziping."
        return
    fi
    wget http://downloads.raspberrypi.org/raspbian/images/raspbian-2018-04-19/2018-04-18-raspbian-stretch.zip
    unzip $RPI_ZIP_FILE
}

mkdir -p ~/opt
cd ~/opt
get_rpi_img
OFFSET=$(sudo parted $RPI_IMG_FILE unit s print | grep ext4 | awk '{print $2}')
NEW_OFFSET=${OFFSET%s*}
OFFSET=$(( $NEW_OFFSET * $BLOCK_SIZE ))
sudo mkdir -p /mnt/rasp-pi-rootfs
sudo mount -o loop,offset="$OFFSET" $RPI_IMG_FILE /mnt/rasp-pi-rootfs
get_toolchain_tools
get_darius_toolchain
get_qt5_module
cd ~/opt/"$DARIUS_TOOLCHAIN"
./fixQualifiedLibraryPaths /mnt/rasp-pi-rootfs/ ~/opt/"$TOOLCHAIN"/arm-bcm2708/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc
echo "** Moving to Qt5 base dir to configure"
cd ~/opt/"$QT"/qtbase
echo ** Configuring QT5
sudo ./configure -v -opengl es2 -device  linux-rasp-pi3-g++ -device-option \
    CROSS_COMPILE=~/opt/"$TOOLCHAIN"/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf- -sysroot /mnt/rasp-pi-rootfs \
    -opensource -confirm-license -optimized-qmake -reduce-exports -release -make \
    libs -prefix /usr/local/qt5pi -hostprefix /usr/local/qt5pi -no-use-gold-linker
make -j 4
make install
